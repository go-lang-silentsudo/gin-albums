package main

import "github.com/gin-gonic/gin"
import "go-gin-example/web"

func main() {
	router := gin.Default()

	router.GET("/", web.GetAlbumsApi)
	router.POST("/", web.AddAlbumApi)
	router.GET("/albums/:id", web.GetAlbumsByIdApi)

	router.Run("localhost:8080")
}
