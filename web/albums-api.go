package web

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"go-gin-example/models"
	"net/http"
	"sort"
)

func GetAlbumsApi(c *gin.Context) {
	albums := models.GetAlbums()
	sort.Slice(albums, func(i, j int) bool {
		return albums[i].ID < albums[j].ID
	})
	c.IndentedJSON(http.StatusOK, albums)
}
func GetAlbumsByIdApi(c *gin.Context) {
	id := c.Param("id")
	item, exists := models.GetAlbumsById(id)
	if exists {
		c.IndentedJSON(http.StatusOK, item)
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"message": fmt.Sprintf("Album with id %s Not Found", id)})
	}
}

func AddAlbumApi(c *gin.Context) {
	var newAlbum models.Album

	err := c.BindJSON(&newAlbum)
	if err != nil {
		fmt.Println("Error adding new album")
		_ = c.Error(err)
	}
	models.AddAlbum(newAlbum)
	c.IndentedJSON(http.StatusCreated, newAlbum)
}
